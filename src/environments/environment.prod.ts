import { Environment } from './environment.type'

export const environment: Environment = {
  production: true,
  webSocketServer: {
    baseUrl: 'ws://0.0.0.0:8080', // TODO
  },
  api: {
    baseUrl: 'http://0.0.0.0:8080', // TODO
  },
}
