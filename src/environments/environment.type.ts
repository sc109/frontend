export interface Environment {
  production: boolean
  webSocketServer: {
    baseUrl: string
  }
  api: {
    baseUrl: string
  }
}
