import { Directive, Injector, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Subject } from 'rxjs'
import { MeService } from '../services/me.service'
import { User } from '../types/user.type'

@Directive({
  selector: '[oggyBaseComponent]',
})
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export class BaseComponent implements OnDestroy, OnInit {
  me: User | undefined
  protected activatedRoute: ActivatedRoute
  protected meService: MeService
  protected unsubscribeAll$ = new Subject()

  constructor(injector: Injector) {
    this.activatedRoute = injector.get(ActivatedRoute)
    this.meService = injector.get(MeService)
  }

  ngOnInit(): void {
    this.me = this.meService.me
  }

  ngOnDestroy(): void {
    this.unsubscribeAll$.next(null)
    this.unsubscribeAll$.complete()
  }

  protected getQueryParams(paramName: string): string | null {
    return this.activatedRoute.snapshot.queryParamMap.get(paramName)
  }

  protected getRouteParam(paramName: string): string | null {
    return this.findRouteParam(paramName, this.activatedRoute)
  }

  private findRouteParam(
    paramName: string,
    route: ActivatedRoute
  ): string | null {
    if (route.snapshot.paramMap.has(paramName)) {
      return route.snapshot.paramMap.get(paramName)
    } else if (!route.parent) {
      return null
    }
    return this.findRouteParam(paramName, route.parent)
  }
}
