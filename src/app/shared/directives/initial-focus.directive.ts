import { Directive, ElementRef, OnInit } from '@angular/core'

@Directive({
  selector: '[oggyInitialFocus]',
})
export class InitialFocusDirective implements OnInit {
  constructor(private hostElement: ElementRef) {}

  ngOnInit(): void {
    this.hostElement.nativeElement.focus()
  }
}
