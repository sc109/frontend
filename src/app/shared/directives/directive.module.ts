import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { InitialFocusDirective } from './initial-focus.directive'
import { BaseComponent } from './base-component.directive'

@NgModule({
  declarations: [InitialFocusDirective, BaseComponent],
  imports: [CommonModule],
  exports: [InitialFocusDirective, BaseComponent],
})
export class DirectiveModule {}
