import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'oggy-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent {}
