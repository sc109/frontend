import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MainLayoutComponent } from './main-layout/main-layout.component'
import { RouterModule } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [CommonModule, RouterModule, BrowserModule, BrowserAnimationsModule],
  exports: [MainLayoutComponent],
})
export class LayoutModule {}
