import { User } from './user.type'

export interface OggyMessage {
  kind: 'MESSAGE' | 'INIT_ROOM'
  channel: string
  text: string
  timestamp: number
  author?: User
}
