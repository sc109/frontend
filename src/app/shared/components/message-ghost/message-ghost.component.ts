import { Component } from '@angular/core'

@Component({
  selector: 'oggy-message-ghost',
  templateUrl: './message-ghost.component.html',
  styleUrls: ['./message-ghost.component.scss'],
})
export class MessageGhostComponent {}
