import { Component, Input } from '@angular/core'

@Component({
  selector: 'oggy-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input() title!: string
  @Input() description!: string
}
