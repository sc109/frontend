import { Component, ElementRef, Input, OnInit } from '@angular/core'

@Component({
  selector: 'oggy-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {
  @Input() type: 'INFO' | 'WARNING' | 'ERROR' = 'INFO'

  get host() {
    return this.element.nativeElement
  }

  constructor(private element: ElementRef) {}

  ngOnInit(): void {
    this.host.classList.add(this.type.toLocaleLowerCase())
  }
}
