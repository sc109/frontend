import { Component, Input, OnInit } from '@angular/core'
import { first, Observable } from 'rxjs'
import { MeService } from '../../services/me.service'
import { OggyMessage } from '../../types/message.type'
import { User } from '../../types/user.type'

@Component({
  selector: 'oggy-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  @Input() message!: OggyMessage

  constructor(public meService: MeService) {}
}
