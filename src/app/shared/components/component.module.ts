import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MessageComponent } from './message/message.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { PipeModule } from '../pipes/pipe.module'
import { NavbarComponent } from './navbar/navbar.component'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { CreateUserDialogComponent } from './create-user-dialog/create-user-dialog.component'
import { MatDialogModule } from '@angular/material/dialog'
import { A11yModule } from '@angular/cdk/a11y'
import { RouterModule } from '@angular/router'
import { SwiperModule } from 'swiper/angular'
import { MatTooltipModule } from '@angular/material/tooltip'
import { MatInputModule } from '@angular/material/input'
import { InputComponent } from './input/input.component'
import { NavbarItemComponent } from './navbar/navbar-item/navbar-item.component'
import { OggyButtonComponent } from './oggy-button/oggy-button.component'
import { HeaderComponent } from './header/header.component'
import { AlertComponent } from './alert/alert.component'
import { MessageGhostComponent } from './message-ghost/message-ghost.component'

@NgModule({
  declarations: [
    MessageComponent,
    NavbarComponent,
    CreateUserDialogComponent,
    InputComponent,
    NavbarItemComponent,
    OggyButtonComponent,
    HeaderComponent,
    AlertComponent,
    MessageGhostComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PipeModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    A11yModule,
    RouterModule,
    SwiperModule,
    MatTooltipModule,
    MatInputModule,
  ],
  exports: [
    MessageComponent,
    InputComponent,
    NavbarComponent,
    OggyButtonComponent,
    HeaderComponent,
    AlertComponent,
    MessageGhostComponent,
  ],
})
export class ComponentModule {}
