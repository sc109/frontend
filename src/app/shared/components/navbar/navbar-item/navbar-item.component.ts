import { Component, Input } from '@angular/core'

@Component({
  selector: 'oggy-navbar-item',
  templateUrl: './navbar-item.component.html',
  styleUrls: ['./navbar-item.component.scss'],
})
export class NavbarItemComponent {
  @Input() link: string[] = []

  @Input() tooltip: string = ''
}
