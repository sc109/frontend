import { Component, OnInit } from '@angular/core'
import { SwiperOptions } from 'swiper'
import SwiperCore, { FreeMode, Mousewheel } from 'swiper'
import { ChatRoomService } from '../../services/chat-room.service'
import { ChatRoom } from '../../types/chat-room.type'
import { Observable } from 'rxjs'

SwiperCore.use([FreeMode, Mousewheel])

@Component({
  selector: 'oggy-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  config: SwiperOptions = {
    direction: 'vertical',
    spaceBetween: 50,
    scrollbar: false,
  }

  chatRooms$!: Observable<ChatRoom[]>

  constructor(private chatRoomService: ChatRoomService) {}

  ngOnInit(): void {
    this.chatRoomService.findAll().subscribe()
    this.chatRooms$ = this.chatRoomService.chatrooms$
  }
}
