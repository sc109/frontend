import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { MatDialog, MatDialogRef } from '@angular/material/dialog'

@Component({
  selector: 'oggy-create-user-dialog',
  templateUrl: './create-user-dialog.component.html',
  styleUrls: ['./create-user-dialog.component.scss'],
})
export class CreateUserDialogComponent implements OnInit {
  formGroup!: FormGroup

  constructor(
    public dialogRef: MatDialogRef<CreateUserDialogComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      id: [],
      pseudo: ['', Validators.required],
    })
  }

  submit() {
    if (this.formGroup.valid) {
      const value = this.formGroup.value
      this.dialogRef.close(value)
    }
  }
}
