import { Component } from '@angular/core'

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'a[oggyButton], button[oggyButton]',
  templateUrl: './oggy-button.component.html',
  styleUrls: ['./oggy-button.component.scss'],
})
export class OggyButtonComponent {}
