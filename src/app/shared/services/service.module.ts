import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { EnvironmentService } from './environment.service'
import { MeService } from './me.service'
import { ErrorService } from './error.service'
import { WebsocketService } from './websocket.service'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatDialogModule } from '@angular/material/dialog'
import { HttpClientModule } from '@angular/common/http'
import { ApiService } from './api.service'
import { ChatRoomService } from './chat-room.service'

@NgModule({
  imports: [MatSnackBarModule, MatDialogModule, HttpClientModule],
  providers: [
    EnvironmentService,
    MeService,
    ErrorService,
    WebsocketService,
    ApiService,
    ChatRoomService,
  ],
})
export class ServiceModule {}
