import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import {
  BehaviorSubject,
  catchError,
  Observable,
  of,
  switchMap,
  tap,
} from 'rxjs'
import { ChatRoom } from '../types/chat-room.type'
import { OggyMessage } from '../types/message.type'
import { ApiService } from './api.service'
import { ErrorService } from './error.service'
import { MeService } from './me.service'

@Injectable()
export class ChatRoomService {
  chatrooms$ = new BehaviorSubject<ChatRoom[]>([])

  constructor(
    private http: HttpClient,
    private apiService: ApiService,
    private errorService: ErrorService,
    private meService: MeService
  ) {}

  create(chatRoom: ChatRoom): Observable<void> {
    return this.http
      .post<void>(
        `${this.apiService.endpoint()}/api/user/${
          this.meService.me.pseudo
        }/room`,
        chatRoom
      )
      .pipe(
        catchError((e) => {
          console.log('-------> ', e)
          this.errorService.handleError('Cannot create chatroom')
          return of()
        })
      )
  }

  findAll(): Observable<ChatRoom[]> {
    return this.http
      .get<ChatRoom[]>(
        `${this.apiService.endpoint()}/api/user/${
          this.meService.me.pseudo
        }/room`
      )
      .pipe(
        tap((chatRooms) => this.chatrooms$.next(chatRooms)),
        catchError(() => {
          this.errorService.handleError('Cannot fetch chatrooms')
          return of([])
        })
      )
  }

  get(slug: string): Observable<ChatRoom> {
    return this.http
      .get<ChatRoom>(`${this.apiService.endpoint()}/api/room/${slug}`)
      .pipe(
        catchError(() => {
          this.errorService.handleError('Cannot fetch room')
          return of()
        })
      )
  }

  findAllMessagesByRoom(chatRoom: ChatRoom): Observable<OggyMessage[]> {
    return this.http
      .get<OggyMessage[]>(
        `${this.apiService.endpoint()}/api/message/${chatRoom.slug}`
      )
      .pipe(
        catchError(() => {
          this.errorService.handleError('Cannot fetch messages')
          return of([])
        })
      )
  }
}
