import { Injectable } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { BehaviorSubject, switchMap, tap } from 'rxjs'
import { CreateUserDialogComponent } from '../components/create-user-dialog/create-user-dialog.component'
import { User } from '../types/user.type'

@Injectable()
export class MeService {
  private _me$!: BehaviorSubject<User>

  constructor(private matDialog: MatDialog) {}

  get me(): User {
    return this._me$.getValue()
  }

  get me$() {
    if (!this._me$) {
      return this.matDialog
        .open(CreateUserDialogComponent, {
          disableClose: true,
        })
        .afterClosed()
        .pipe(
          switchMap((user) => {
            this._me$ = new BehaviorSubject(user)
            return this._me$
          })
        )
    }
    return this._me$
  }

  isMe(user: User | undefined): boolean {
    return user?.pseudo === this.me.pseudo
  }
}
