import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ChatRoom } from '../types/chat-room.type'
import { EnvironmentService } from './environment.service'

@Injectable()
export class ApiService {
  constructor(private environmentService: EnvironmentService) {}

  endpoint() {
    return this.environmentService.environment.api.baseUrl
  }
}
