import { Injectable } from '@angular/core'
import {
  BehaviorSubject,
  catchError,
  concatMap,
  delay,
  Observable,
  of,
  retry,
  retryWhen,
  Subject,
  Subscription,
  switchMap,
  take,
  throwError,
  timer,
} from 'rxjs'
import { webSocket, WebSocketSubject } from 'rxjs/webSocket'
import { OggyMessage } from '../types/message.type'
import { User } from '../types/user.type'
import { EnvironmentService } from './environment.service'
import { ErrorService } from './error.service'
import { MeService } from './me.service'

@Injectable()
export class WebsocketService {
  conexionStatus$ = new BehaviorSubject(false)

  private subject!: WebSocketSubject<OggyMessage> | null

  messages$ = new Subject<OggyMessage>()

  initConnection(me: User) {
    this.subject = webSocket({
      url: `${this.environmentService.environment.webSocketServer.baseUrl}/oggy/${me.pseudo}`,
      openObserver: {
        next: this.openObserver.bind(this),
      },
      closeObserver: {
        next: this.closeObserver.bind(this),
      },
    })

    this.subject
      .pipe(
        retry({
          delay: 1000,
        }),
        catchError((e) => {
          this.errorService.handleError('Connection error')
          return of()
        })
      )
      .subscribe({
        next: (message) => this.messages$.next(message),
      })
  }

  constructor(
    private environmentService: EnvironmentService,
    private errorService: ErrorService,
    private meService: MeService
  ) {}

  private openObserver() {
    console.log('Open')
    this.conexionStatus$?.next(true)
  }

  private closeObserver() {
    console.log('Close')
    this.conexionStatus$?.next(false)
  }

  send(message: OggyMessage): void {
    if (this.subject) {
      this.subject.next(message)
    }
  }
}
