import { Injectable, OnInit } from '@angular/core'
import { MatSnackBar, MatSnackBarDismiss } from '@angular/material/snack-bar'
import { Observable } from 'rxjs'

@Injectable()
export class ErrorService {
  constructor(private matSnackBar: MatSnackBar) {}

  handleError(message: string) {
    this.displaySnackBar(message).subscribe()
  }

  private displaySnackBar(message: string): Observable<MatSnackBarDismiss> {
    return this.matSnackBar
      .open(message, 'close', {
        duration: 2000,
      })
      .afterDismissed()
  }
}
