import { Pipe, PipeTransform } from '@angular/core'
import { User } from '../types/user.type'

@Pipe({
  name: 'displayUser',
})
export class DisplayUserPipe implements PipeTransform {
  transform(user: User | undefined): string {
    return user ? `${user?.pseudo}` : 'NaN'
  }
}
