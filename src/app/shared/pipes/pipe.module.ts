import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DisplayUserPipe } from './display-user.pipe'
import { ReversePipe } from './reverse.pipe'
import { OrderByPipe } from './order-by.pipe'

@NgModule({
  declarations: [DisplayUserPipe, ReversePipe, OrderByPipe],
  imports: [CommonModule],
  exports: [DisplayUserPipe, ReversePipe, OrderByPipe],
})
export class PipeModule {}
