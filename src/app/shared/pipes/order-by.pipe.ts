import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
  transform(tabs: any[], field: string): any[] {
    return tabs.sort((a, b) => a[field] - b[field])
  }
}
