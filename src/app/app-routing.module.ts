import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ChatRoomFormComponent } from './pages/chat-room-form/chat-room-form.component'
import { ChatRoomComponent } from './pages/chat-room/chat-room.component'
import { HomeComponent } from './pages/home/home.component'
import { ProfileComponent } from './pages/profile/profile.component'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: ProfileComponent,
      },
      {
        path: 'room/chat/:id',
        component: ChatRoomComponent,
      },
      {
        path: 'room/new',
        component: ChatRoomFormComponent,
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: '',
      },
    ],
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'room',
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
