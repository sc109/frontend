import { LOCALE_ID, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { PageModule } from './pages/pages.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ServiceModule } from './shared/services/service.module'
import { registerLocaleData } from '@angular/common'
import localeFr from '@angular/common/locales/fr'

registerLocaleData(localeFr, 'fr')

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PageModule,
    BrowserAnimationsModule,
    ServiceModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'fr' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
