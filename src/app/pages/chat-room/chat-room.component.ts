import { Component, Injector, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { ParamMap } from '@angular/router'
import { AnimationOptions } from 'ngx-lottie'
import {
  combineLatest,
  delay,
  filter,
  finalize,
  first,
  iif,
  map,
  mergeMap,
  Observable,
  of,
  scan,
  startWith,
  switchMap,
  take,
  takeUntil,
  tap,
} from 'rxjs'
import { BaseComponent } from 'src/app/shared/directives/base-component.directive'
import { ChatRoomService } from 'src/app/shared/services/chat-room.service'
import { WebsocketService } from 'src/app/shared/services/websocket.service'
import { ChatRoom } from 'src/app/shared/types/chat-room.type'
import { OggyMessage } from 'src/app/shared/types/message.type'

@Component({
  selector: 'oggy-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss'],
})
export class ChatRoomComponent extends BaseComponent implements OnInit {
  messages: OggyMessage[] = []
  chatRoom!: ChatRoom
  conexionStatus$!: Observable<boolean>
  formGroup!: FormGroup
  ghosts!: number[]

  options: AnimationOptions = {
    path: 'assets/lotties/loading-files.json',
  }

  constructor(
    private websocketService: WebsocketService,
    private fb: FormBuilder,
    private chatRoomService: ChatRoomService,
    protected injector: Injector
  ) {
    super(injector)
  }

  override ngOnInit(): void {
    super.ngOnInit()
    this.conexionStatus$ = this.websocketService.conexionStatus$
    this.initChatRoomMessages()
  }

  private initChatRoomMessages() {
    const onChatRoomChange = (chatRoomSlug: string): void => {
      if (chatRoomSlug !== this.chatRoom?.slug) {
        this.messages = []
      }
    }

    const fetchChatRoom$ = (chatRoomSlug: string): Observable<ChatRoom> =>
      this.chatRoomService.get(chatRoomSlug).pipe(
        tap((chatRoom: ChatRoom) => {
          this.chatRoom = chatRoom
          this.initForm()
        })
      )

    const fetchOldMessage$ = (chatRoom: ChatRoom): Observable<OggyMessage[]> =>
      this.chatRoomService.findAllMessagesByRoom(chatRoom).pipe(
        tap((messages: OggyMessage[]) => {
          console.log(messages)
          this.messages.push(...messages)
        }),
        finalize(() => this.toggleGhost(false))
      )

    const receiveNewMessages$ = () =>
      this.websocketService.messages$.pipe(
        filter(
          (message: OggyMessage) =>
            message.kind === 'MESSAGE' && message.channel === this.chatRoom.slug
        )
      )

    this.activatedRoute.paramMap
      .pipe(
        tap(() => this.toggleGhost(true)),
        map((paramMap: ParamMap) => paramMap.get('id')),
        filter((id: any) => !!id),
        tap(onChatRoomChange),
        switchMap(fetchChatRoom$),
        switchMap(fetchOldMessage$),
        switchMap(receiveNewMessages$),
        takeUntil(this.unsubscribeAll$)
      )
      .subscribe((oggyMessage: OggyMessage) => {
        this.messages = this.messages.concat(oggyMessage)
      })
  }

  submit(): void {
    const values: OggyMessage = this.formGroup.value
    values.timestamp = Date.now()

    if (values && values.text?.length) {
      this.formGroup.get('text')?.reset()
      this.websocketService.send(values)
    }
  }

  private toggleGhost(status: boolean) {
    this.ghosts = status ? new Array(6).fill(0) : []
  }

  private initForm(): void {
    this.formGroup = this.fb.group({
      kind: ['MESSAGE'],
      channel: [this.chatRoom.slug],
      author: [this.me],
      text: [''],
    })
  }
}
