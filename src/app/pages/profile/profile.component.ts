import { Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { MeService } from 'src/app/shared/services/me.service'
import { User } from 'src/app/shared/types/user.type'

@Component({
  selector: 'oggy-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  me$!: Observable<User>

  constructor(private meService: MeService) {}

  ngOnInit(): void {
    this.me$ = this.meService.me$
  }
}
