import { Component, OnInit } from '@angular/core'
import { AnimationOptions } from 'ngx-lottie'
import { Observable, tap } from 'rxjs'
import { MeService } from 'src/app/shared/services/me.service'
import { WebsocketService } from 'src/app/shared/services/websocket.service'
import { User } from 'src/app/shared/types/user.type'
@Component({
  selector: 'oggy-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  me$!: Observable<User>

  options: AnimationOptions = {
    path: 'assets/lotties/nyan-cat.json',
  }

  constructor(
    private meService: MeService,
    private websocketService: WebsocketService
  ) {}

  ngOnInit(): void {
    this.me$ = this.meService.me$.pipe(
      tap((me) => this.websocketService.initConnection(me))
    )
  }
}
