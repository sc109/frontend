import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HomeComponent } from './home/home.component'
import { ComponentModule } from '../shared/components/component.module'
import { LayoutModule } from '../shared/layouts/layout.module'
import { ProfileComponent } from './profile/profile.component'
import { ChatRoomComponent } from './chat-room/chat-room.component'
import { PipeModule } from '../shared/pipes/pipe.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { LottieModule } from 'ngx-lottie'
import player from 'lottie-web'
import { ChatRoomFormComponent } from './chat-room-form/chat-room-form.component'
import { DirectiveModule } from '../shared/directives/directive.module'
import { RouterModule } from '@angular/router'

export function playerFactory() {
  return player
}

@NgModule({
  declarations: [
    HomeComponent,
    ProfileComponent,
    ChatRoomComponent,
    ChatRoomFormComponent,
  ],
  imports: [
    CommonModule,
    ComponentModule,
    RouterModule,
    LayoutModule,
    ReactiveFormsModule,
    PipeModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    DirectiveModule,
    LottieModule.forRoot({ player: playerFactory }),
  ],
})
export class PageModule {}
