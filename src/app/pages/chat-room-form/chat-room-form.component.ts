import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { switchMap } from 'rxjs'
import slugify from 'slugify'
import { ChatRoomService } from 'src/app/shared/services/chat-room.service'
import { ChatRoom } from 'src/app/shared/types/chat-room.type'

@Component({
  selector: 'oggy-chat-room-form',
  templateUrl: './chat-room-form.component.html',
  styleUrls: ['./chat-room-form.component.scss'],
})
export class ChatRoomFormComponent implements OnInit {
  formGroup!: FormGroup

  constructor(
    private fb: FormBuilder,
    private chatRoomService: ChatRoomService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      slug: [''],
    })
  }

  submit(): void {
    if (this.formGroup.valid) {
      const room: ChatRoom = this.formGroup.value
      room.slug = slugify(room.slug)
      this.chatRoomService
        .create(room)
        .pipe(switchMap(() => this.chatRoomService.findAll()))
        .subscribe(() => {
          this.router.navigate(['/'])
        })
    }
  }
}
