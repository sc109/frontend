# Oggy - Frontend

This repository contains the Angular source code for the chat application of the [2022 FRP Project](https://docs.google.com/document/d/1anV0G7cVZiEljeEf_qi-5mF_ErKitjxJkAWrq0JjRXY/edit#heading=h.fylfh8oec611).

## Requirements

- [Node.js](https://nodejs.org/en/)

## Local installation

```sh
# Clone the project
git clone git@gitlab.polytech.umontpellier.fr:akk-chat/frontend.git
cd frontend
# Install dependencies
npm i
# Start the development server
npm start
```

### Format the source code

```sh
npm run format
```

### Check for code quality

```sh
npm run lint
```

## Authors

- [Julian Labatut](mailto:julian.labatut@etu.umontpellier.fr)
- [Kalil Pelissier](mailto:kalil.pelissier@etu.umontpellier.fr)
- [Malo Polese](mailto:malo.polese@etu.umontpellier.fr)
